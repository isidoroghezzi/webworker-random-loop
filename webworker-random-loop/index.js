/*
    Milano (MI) Italy
    Sat Dec  7 14:29:41 CET 2013
    Copyright 2013 - 2019, Isidoro Ghezzi
    isidoro.ghezzi@icloud.com
*/

(function () {
	'use strict';
	document.addEventListener('DOMContentLoaded', _ => {
		const ClassController = function (theDate) {
			const self = this;
			self.numberOfLoops = 100000000 * 10;
			// self.numberOfLoops = 10;
			self.startCrono = null;

			self.newTestModel = function () {
				const ret = {
					deltaSeconds: {
						cumulative: null,
						net: null,
						gross: null
					},
					array: [
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0
					]
				};
				return ret;
			};

			self.util = {
				keys(theObject) {
					const ret = [];
					for (const aProperty in theObject) {
						/* eslint-disable-next-line no-prototype-builtins */
						if (theObject.hasOwnProperty(aProperty)) {
							ret.push(aProperty);
						}
					}

					return ret;
				},
				createElement(theElement) {
					const ret = document.createElement(theElement.tag);
					if ((typeof undefined !== typeof theElement.properties) && (theElement.properties !== null)) {
						this.keys(theElement.properties).forEach(theProperty => {
							ret[theProperty] = theElement.properties[theProperty];
						});
					}

					return ret;
				},
				newCrono() {
					const startDate = new Date();
					return {
						deltaSeconds() {
							const endDate = new Date();
							return (endDate.getTime() - startDate.getTime()) / 1000;
						}
					};
				}
			};

			self.test = null;

			self.updateUI = function (theFlagSimple) {
				return function () {
					const $wrapper = self.util.createElement({tag: 'div', properties: {id: 'resultWrapper'}});
					const $table = self.util.createElement({tag: 'table', properties: {id: 'result'}});
					let aSum = 0;
					let $sigmaRow = null;
					let $testResult = null;
					let $userAgent = null;
					let indexRow = 0;
					$table.classList.add('result');
					self.test.array.forEach(valueRow => {
						const $row = self.util.createElement({tag: 'tr'});
						++indexRow;
						aSum += valueRow;
						[indexRow, valueRow].forEach(rowElement => {
							const $element = self.util.createElement({tag: 'td', properties: {textContent: rowElement}});
							$row.append($element);
						});
						$table.append($row);
					});
					$sigmaRow = self.util.createElement({tag: 'tr', properties: {
						innerHTML: '<td>&Sigma;</td><td>' + aSum + '</td>'
					}});
					$sigmaRow.style.content = 'border-top: 1px solid green';
					$table.append($sigmaRow);
					$wrapper.append($table);
					let aNumberOfWorkerIf = '';
					if (theFlagSimple === true) {
						aNumberOfWorkerIf = '(number of worker: ' + self.numWorker + ') ';
					}

					$testResult = self.util.createElement({tag: 'div', properties: {
						innerHTML: String(aSum) + ' loops executed ' + aNumberOfWorkerIf + 'in <span id="seconds" class="seconds">' + JSON.stringify(self.test.deltaSeconds) + '</span> seconds'
					}});
					$userAgent = self.util.createElement({tag: 'div', properties: {
						textContent: 'userAgent: ' + navigator.userAgent
					}});
					[$testResult, $userAgent].forEach($value => {
						$wrapper.append($value);
					});
					$wrapper.append($testResult);
					document.querySelector('#now').textContent = new Date().toString();
					document.querySelector('#resultWrapper').remove();
					document.querySelector('#content').append($wrapper);
				};
			};

			/*
			self.loopTest = function () {
				const aCrono = self.util.newCrono();
				const aArray = [
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				];
				const aLength = self.test.array.length;
				const {numberOfLoops} = self;
				for (let i = 0; i < numberOfLoops; ++i) {
					const pseudoRandomIndex = Math.floor(Math.random() * aLength);
					++aArray[pseudoRandomIndex];
				}

				self.test.deltaSeconds = aCrono.deltaSeconds();
				self.test.array = aArray;
				setTimeout(self.updateUI(false), 0);
				document.querySelector('#testButton').classList.add('normal');
				document.querySelector('#testButton').classList.remove('busy');
			};

			self.clickTest = function (theId) {
				return function (_) {
					document.querySelector(theId).classList.add('busy');
					document.querySelector(theId).classList.remove('normal');
					document.querySelector('#resultWrapper').textContent = 'running...';
					self.test = self.newTestModel();
					setTimeout(() => {
						self.loopTest();
					}, 1000);
				};
			};
*/
			self.vectorialOperation = function (v1, v2, binaryOperation) {
				const ret = [];
				for (const [i, element] of v1.entries()) {
					ret.push(binaryOperation(element, v2[i]));
				}

				return ret;
			};

			self.vectorialSum = function (v1, v2) {
				return self.vectorialOperation(v1, v2, (a, b) => {
					return a + b;
				});
			};

			self.matrixAccumulateWithExtraxtorAndOperation = function (theMatrix, extractor, binaryOperation) {
				const ret = [];
				extractor(theMatrix[0]).forEach(() => {
					ret.push(0);
				});
				theMatrix.forEach(theVector => {
					for (let i = 0; i < ret.length; ++i) {
						ret[i] = binaryOperation(ret[i], extractor(theVector)[i]);
					}
				});
				return ret;
			};

			self.matrixAccumulateWithExtractor = function (theMatrix, extractor) {
				return self.matrixAccumulateWithExtraxtorAndOperation(theMatrix, extractor, (a, b) => {
					return a + b;
				});
			};

			self.vectorAccumulateWithExtraxtorAndOperation = function (theVector, extractor, binaryOperation) {
				let ret = 0;

				theVector.forEach(theElement => {
					ret = binaryOperation(ret, extractor(theElement));
				});
				return ret;
			};

			self.vectorAccumulateWithExtractor = function (theVector, extractor) {
				return self.vectorAccumulateWithExtraxtorAndOperation(theVector, extractor, (a, b) => {
					return a + b;
				});
			};

			self.handleMessageFromWorker = function (theId, theIndex, theNumWorker) {
				return function (theEvent) {
					self.workerHandler.tests.push(theEvent.data);
					console.log('self.workerHandler.tests.length: ' + self.workerHandler.tests.length);
					console.log(JSON.stringify({theIndex, theEvent: theEvent.data}));
					document.querySelector('#resultWrapper').textContent += ' - ' + self.workerHandler.tests.length;
					if (theNumWorker === self.workerHandler.tests.length) {
						self.test = self.newTestModel();
						self.test.deltaSeconds.net = self.workerHandler.crono.deltaSeconds();

						self.test.array = self.matrixAccumulateWithExtractor(self.workerHandler.tests, matrix => {
							return matrix.array;
						});

						self.test.deltaSeconds.cumulative = self.vectorAccumulateWithExtractor(self.workerHandler.tests, element => {
							return element.deltaSeconds;
						});

						setTimeout(self.updateUI(true), 0);
						document.querySelector(theId).classList.add('normal');
						document.querySelector(theId).classList.remove('busy');
						self.workerHandler.workers.forEach(worker => {
							worker.terminate();
						});
						self.test.deltaSeconds.gross = self.startCrono.deltaSeconds();
					}
				};
			};

			self.handleErrorFromWorker = function (theId, theIndex, theNumWorker) {
				return function (theEvent) {
					console.log(JSON.stringify({message: theEvent.message, filename: theEvent.filename, lineno: theEvent.lineno, id: theId, index: theIndex, numWorker: theNumWorker}));
				};
			};

			self.newWorkerHandler = function (theId, theNumWorker) {
				const workers = [];
				for (let i = 0; i < theNumWorker; ++i) {
					const aWorker = new Worker('worker.js');
					aWorker.addEventListener('message', self.handleMessageFromWorker(theId, i, theNumWorker));
					aWorker.addEventListener('error', self.handleErrorFromWorker(theId, i, theNumWorker));
					workers.push(aWorker);
				}

				return {
					workers,
					crono: null,
					tests: []
				};
			};

			self.clickTestWorker = function (theId) {
				return function (_) {
					self.startCrono = self.util.newCrono();
					const aSelectElement = document.querySelector('#numWorkers');
					self.numWorker = parseInt(aSelectElement.value, 10);
					self.workerHandler = self.newWorkerHandler('#testButtonWorker', self.numWorker);
					document.querySelector(theId).classList.add('busy');
					document.querySelector(theId).classList.remove('normal');
					document.querySelector('#resultWrapper').textContent = 'running...';
					// SetTimeout(function () {
					self.workerHandler.crono = self.util.newCrono();
					self.workerHandler.tests = [];
					self.workerHandler.workers.forEach((theWorker, theIndex) => {
						theWorker.postMessage({index: theIndex, numberOfLoops: self.numberOfLoops / self.numWorker, test: self.newTestModel()});
					});
					// }, 0);
				};
			};

			self.getYears = () => {
				const currentYear = new Date().getFullYear();
				const years = [];
				for (let year = 2013; year <= currentYear; ++year) {
					years.push(year);
				}

				return years.join(' - ');
			};

			self.showInputWorkNumber = function (number) {
				const options = [];
				for (let i = 0; i < number; ++i) {
					const $option = self.util.createElement({
						tag: 'option',
						properties: {
							value: i + 1,
							textContent: i + 1
						}
					});
					options.push($option);
				}

				const $select = self.util.createElement({
					tag: 'select',
					properties: {
						id: 'numWorkers'
					}
				});
				$select.classList.add('numWorkers');
				options.forEach(option => {
					$select.append(option);
				});
				document.querySelector('#workers').append($select);
			};

			self.show = function () {
				const $wrapper = self.util.createElement({
					tag: 'div',
					properties: {
						id: 'resultWrapper',
						textContent: 'number of loops: ' + self.numberOfLoops + ';'
					}
				});

				document.querySelector('#now').textContent = theDate;
				[{id: '#testButtonWorker', onclicker: self.clickTestWorker}].forEach(theButton => {
					const aDOM = document.querySelector(theButton.id);
					aDOM.classList.add('normal');
					aDOM.addEventListener('click', theButton.onclicker(theButton.id));
				});

				document.querySelector('#content').append($wrapper);

				self.showInputWorkNumber(16);
				document.querySelector('#years').textContent = self.getYears();
			};
		};

		const aController = new ClassController(new Date());
		aController.show();
	});
})();
