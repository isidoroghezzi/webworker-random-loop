/* eslint-disable no-unused-vars */

/*
    Almaty, Kazakistan
    Sun Dec  1 16:24:33 +06 2019
    Copyright 2019, Isidoro Ghezzi
    isidoro.ghezzi@icloud.com
*/
const newCrono = function () {
	const startDate = new Date();
	return {
		deltaSeconds() {
			const endDate = new Date();
			return (endDate.getTime() - startDate.getTime()) / 1000;
		}
	};
};

const igOnMessageOld = function (e) {
	console.log('worker.js: onmessage started: ' + JSON.stringify(e.data));
	const aLength = e.data.test.array.length;
	for (let i = 0; i < e.data.numberOfLoops; ++i) {
		const pseudoRandomIndex = Math.floor(Math.random() * aLength);
		++e.data.test.array[pseudoRandomIndex];
	}

	postMessage(e.data.test);
	console.log('worker.js:onmessage ended: ' + JSON.stringify(e.data));
};

const onmessageOld = function (e) {
	setTimeout(() => {
		igOnMessage(e);
		close();
	});
};

const igOnMessage = function (e) {
	console.log('worker.js: 2 - onmessage started: ' + JSON.stringify(e.data));
	const aCrono = newCrono();
	const aArray = e.data.test.array;
	/*
	const aArray = [
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	];
	*/
	const aLength = e.data.test.array.length;
	const {numberOfLoops} = e.data;
	for (let i = 0; i < numberOfLoops; ++i) {
		const pseudoRandomIndex = Math.floor(Math.random() * aLength);
		++aArray[pseudoRandomIndex];
	}

	e.data.test.array = aArray;
	e.data.test.deltaSeconds = aCrono.deltaSeconds();
	console.log('worker.js: onmessage ended: ' + JSON.stringify(e.data));
	postMessage(e.data.test);
};

onmessage = function (e) {
	igOnMessage(e);
	close();
};
